# roaster-monitor

## Dev (using nix)

Enter the devshell with `nix develop` or just `direnv allow` if you're using direnv.

### Utils

- `nix fmt`, to format the entire tree.
- `nix flake check`, to ensure formatting is okay and all tests succeed.

## Directory

- TBD
