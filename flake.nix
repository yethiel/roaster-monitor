{
  inputs.dream2nix.url = "github:nix-community/dream2nix";
  inputs.devshell.url = "github:numtide/devshell";
  inputs.flake-parts.url = "github:hercules-ci/flake-parts";
  inputs.treefmt-nix.url = "github:numtide/treefmt-nix";
  inputs.pre-commit-hooks-nix.url = "github:cachix/pre-commit-hooks.nix";
  inputs.nixpkgs.url = "nixpkgs";

  outputs = inputs @ {
    flake-parts,
    dream2nix,
    devshell,
    treefmt-nix,
    pre-commit-hooks-nix,
    ...
  }:
    flake-parts.lib.mkFlake {inherit inputs;} {
      flake = {
        # Put your original flake attributes here.
      };
      imports = [
        dream2nix.flakeModuleBeta
        devshell.flakeModule
        treefmt-nix.flakeModule
        pre-commit-hooks-nix.flakeModule
      ];
      systems = [
        "x86_64-linux"
      ];
      perSystem = {
        self,
        pkgs,
        config,
        ...
      }: {
        dream2nix.inputs = {
          roaster-monitor = {
            source = ./.;
            projects = fromTOML (builtins.readFile ./projects.toml);
          };
        };
        packages = {
          inherit (config.dream2nix.outputs.roaster-monitor.packages) roaster-monitor-backend roaster-monitor-crawler roaster-monitor-shared roaster-monitor-frontend;
          # inherit (config.dream2nix.outputs.roaster-monitor-frontend.packages) roaster-monitor-frontend;
        };
        devShells = {
          default = config.dream2nix.outputs.roaster-monitor.devShells.default.overrideAttrs (old: {
            buildInputs =
              old.buildInputs
              ++ [
                # Additional packages for the shell
                config.treefmt.package
                pkgs.nodejs
                pkgs.yarn
                pkgs.nil
              ];
          });
        };
        treefmt.projectRootFile = "flake.nix";
        treefmt.programs = {
          rustfmt.enable = true;
          prettier.enable = true;
          alejandra.enable = true;
        };
      };
    };
}
